package ru.konovalov.tm.api.service;

import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.User;

public interface IAuthService {
    String getUserId();

    User getUser();

    void checkRoles(Role... roles);

    boolean isAuth();

    void logout();

    void login(String login, String password);

    void registry(String login, String password, String email);
}
