package ru.konovalov.tm.api.service;

import ru.konovalov.tm.api.IService;
import ru.konovalov.tm.enumerated.Role;
import ru.konovalov.tm.model.User;


public interface IUserService extends IService<User> {

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);
    User create(String login, String password, String email);

    User create(String login, String password, Role role);

    User removeById(String id);

    User removeByLogin(String login);

    boolean isLoginExist(String login);

    boolean isEmailExist(String email);

    User add(String login, String password);

    User add(String login, String password, String email);

    User setPassword(String id, String password);

    User updateUser(
            String id,
            String firstName,
            String lastName,
            String middleName
    );

    boolean existsByLogin(String login);

    User lockUserByLogin(String login);

    User unlockUserByLogin(String login);

}
