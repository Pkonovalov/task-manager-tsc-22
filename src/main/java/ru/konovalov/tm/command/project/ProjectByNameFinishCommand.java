package ru.konovalov.tm.command.project;

import ru.konovalov.tm.util.TerminalUtil;

public final class ProjectByNameFinishCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-finish-by-name";
    }

    @Override
    public String description() {
        return "Finish project by name";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[FINISH PROJECT]");
        System.out.println("ENTER NAME:");
        serviceLocator.getProjectService().finishProjectByName(userId, TerminalUtil.nextLine());
    }

}
