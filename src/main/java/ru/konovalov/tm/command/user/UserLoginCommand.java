package ru.konovalov.tm.command.user;

import ru.konovalov.tm.exeption.entity.UserNotFoundException;
import ru.konovalov.tm.util.TerminalUtil;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-login";
    }

    @Override
    public String description() {
        return "Log in to the system";
    }

    @Override
    public void execute() {
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        if (!serviceLocator.getUserService().existsByLogin(login)) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }
}
