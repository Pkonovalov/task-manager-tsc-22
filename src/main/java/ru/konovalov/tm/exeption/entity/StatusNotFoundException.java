package ru.konovalov.tm.exeption.entity;

import ru.konovalov.tm.exeption.AbstractException;

public class StatusNotFoundException extends AbstractException {

    public StatusNotFoundException() {
        super("Error. Task not found.");
    }

    }
