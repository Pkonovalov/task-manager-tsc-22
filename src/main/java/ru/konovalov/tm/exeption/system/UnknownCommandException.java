package ru.konovalov.tm.exeption.system;

import ru.konovalov.tm.constant.TerminalConst;
import ru.konovalov.tm.exeption.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(String command) {
        super("Incorrect command ``"+command+"``. Use " + TerminalConst.CMD_HELP + ", it display list of terminal commands.");
    }

}
