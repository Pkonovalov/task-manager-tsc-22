package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwner> extends AbstractRepository<E> implements IOwnerRepository<E> {

    protected final List<E> list = new ArrayList<>();

    public List<E> findAll(final String userId) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public E findById(final String userId, final String id) {
        return list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .findFirst().orElse(null);
    }

    @Override
    public int size(String userId) {
        int i = 0;
        for (final E e : list) {
            if (userId.equals(e.getUserId())) i++;
        }
        return i;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        for (final E e : list) {
            if (id.equals(e.getId()) && userId.equals(e.getUserId())) return true;
        }
        return false;
    }

    @Override
    public void clear(final String userId) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()))
                .forEach(list::remove);

    }

    @Override
    public Project removeById(final String userId, final String id) {
        list.stream()
                .filter(e -> userId.equals(e.getUserId()) && id.equals(e.getId()))
                .findFirst()
                .ifPresent(this::remove);
        return null;
    }


}