package ru.konovalov.tm.repository;

import ru.konovalov.tm.api.repository.IUserRepository;
import ru.konovalov.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findById(final String id) {
        return list.stream()
                .filter(e -> id.equals(e.getId()))
                .findFirst()
                .orElse(null);

    }

    @Override
    public User findByLogin(final String login) {
        return list.stream()
                .filter(e -> login.equals(e.getLogin()))
                .findFirst()
                .orElse(null);

    }

    @Override
    public User findByEmail(final String email) {
        return list.stream()
                .filter(e -> email.equals(e.getEmail()))
                .findFirst()
                .orElse(null);

    }

    @Override
    public User removeUser(final User user) {
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserById(final String id) {
        final User user = findById(id);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public User removeUserByLogin(final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        list.remove(user);
        return user;
    }

    @Override
    public boolean existsByLogin(final String login) {
        return list.stream()
                .anyMatch(e -> login.equals(e.getLogin()));

    }

    public boolean existsByEmail(final String email) {
        return list.stream()
                .anyMatch(e -> email.equals(e.getEmail()));

    }
}
