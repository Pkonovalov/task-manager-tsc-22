package ru.konovalov.tm.service;

import ru.konovalov.tm.api.IOwnerRepository;
import ru.konovalov.tm.api.IOwnerService;
import ru.konovalov.tm.api.service.IAuthService;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.model.AbstractOwner;
import ru.konovalov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public abstract class AbstractOwnerService<E extends AbstractOwner> extends AbstractService<E> implements IOwnerService<E> {

    protected final IAuthService authService;

    public AbstractOwnerService(final IOwnerRepository<E> repository, IAuthService authService) {
        super(repository);
        this.authService = authService;
        this.repository = repository;
    }

    protected final IOwnerRepository<E> repository;
    public List<E> findAll(final String userId) {
        return repository.findAll(userId);
    }

    public List<E> findAll(final String userId, final Comparator<E> comparator) {
        return repository.findAll(userId, comparator);
    }

    public E findById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public int size(final String userId) {
        if (repository.size() == 0) return 0;
        return repository.size(userId);
    }

    public boolean existsById(final String userId, final String id) {
        if (isEmpty(id)) throw new EmptyIdException();
        return repository.existsById(userId, id);
    }

    @Override
    public void clear(final String userId) {
        repository.clear(userId);
    }

    @Override
    public Project removeById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(userId, id);
        return null;
    }

}
